﻿using UnityEngine;
using System.Collections;

public class SideScrollerCamera : MonoBehaviour 
{	
	public Vector2 offsets;
	private Transform player;		// Reference to the player's transform.
	
	void Awake ()
	{
		// Setting up the reference.
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void FixedUpdate ()
	{
		TrackPlayer();
	}
	
	void TrackPlayer ()
	{
		// By default the target x and y coordinates of the camera are it's current x and y coordinates.
		float targetX = transform.position.x;
		float targetY = transform.position.y;
		
		// ... the target x coordinate should be a Lerp between the camera's current x position and the player's current x position.
		targetX = Mathf.Lerp(transform.position.x, player.position.x, 2 * Time.deltaTime);
		
		// The target x and y coordinates should not be larger than the maximum or smaller than the minimum.
		targetX = Mathf.Clamp(targetX, -5, 5);
		targetY = Mathf.Clamp(targetY, -5, 5);
		
		// Set the camera's position to the target position with the same z component.
		//transform.position = new Vector3(targetX, targetY, transform.position.z);
		transform.position = new Vector3(0, 0, transform.position.z);
	}
}
