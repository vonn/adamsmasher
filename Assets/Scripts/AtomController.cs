﻿using UnityEngine;
using System.Collections;

public enum AtomState {
	BAD, GOOD, NONE
}

public class AtomController : MonoBehaviour 
{
	private GameObject player;	// a handle to the player to orbit
	private bool stateChanged = false; // did my state change this past frame?

	public Sprite goodSprite;
	public Sprite badSprite;
	public float degrees = 5; // how fast to revolve around the player
	public AtomState state = AtomState.BAD; // what state the atom is in

	// Use this for initialization
	void Start () {
		switch (state) {
		case AtomState.BAD:
			this.TurnBad();
			break;
		case AtomState.GOOD:
			this.TurnGood();
			break;
		case AtomState.NONE:
		default:
				break;
		}
	}

	void Awake() 
	{
		degrees += Random.Range(-5, 5);
	}
	
	void Update () 
	{	}

	void FixedUpdate () {
		switch (state) {
		case AtomState.BAD:
			this.BadUpdate();
			break;
		case AtomState.GOOD:
			this.GoodUpdate();
			break;
		case AtomState.NONE:
		default:
			break;
		}	
	}
	
	/// <summary>
	/// The Update for good atoms
	/// </summary>
	void GoodUpdate()
	{
		// orbit around that bad boy
		this.transform.RotateAround (player.transform.position, Vector3.forward, degrees);
	}
	
	/// <summary>
	/// Update method for bad atoms
	/// </summary>
	void BadUpdate()
	{
		return;
	}
	
	/// <summary>
	/// When a bad atom dies, it becomes a good atom.
	/// </summary>
	public void Die() 
	{
		this.TurnGood();
	}
	
	/// <summary>
	/// Sets up the atom to be good, changing the sprite and rotating around the player
	/// </summary>
	public void TurnGood()
	{
		//Debug.Log("Turning good!");
		this.state = AtomState.GOOD;
		this.stateChanged = true;
		
		// change to good atom sprite
		this.GetComponent<SpriteRenderer>().sprite = goodSprite;
		// set up the player reference
		this.player = GameObject.FindGameObjectWithTag ("Player");
		// and become the player's child
		this.transform.parent = player.transform;
		
		// get a certain distance from the player
		float rad = player.GetComponent<PlayerControl>().chargeRadius;
		// pick a random direction 
		Vector3 randTheta = Random.insideUnitSphere.normalized;
		transform.position = player.transform.position + randTheta * rad;
		// increment the player's charge radius
		player.GetComponent<PlayerControl>().chargeRadius += .1f;
	}
	
	/// <summary>
	/// Sets up the atom to be good, changing the sprite and becoming a menace
	/// </summary>
	public void TurnBad()
	{
		//Debug.Log("Turning bad!");
		this.state = AtomState.BAD;
		this.stateChanged = true;
		
		this.GetComponent<SpriteRenderer>().sprite = badSprite;
	}
}
