﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// An Object Pool with initialization and reset.
///
/// Defines a generic pool (stack) of objects that assist in memory management.
/// To obtain a fresh object, call objPool.New(), and store it in the pool
/// with objPool.Store(newObj). You can also define a method to be called when
/// a fresh object is obtained.
/// </summary>
public class ObjectPool<T> where T : class, new()
{
	private Stack<T> m_objectStack = new Stack<T>();
	
	private Action<T> m_resetAction;
	private Action<T> m_onetimeInitAction;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="ObjectPool`1"/> class.
	/// </summary>
	/// <param name="initialBufferSize">Initial buffer size.</param>
	/// <param name="ResetAction">Reset action.</param>
	/// <param name="OnetimeInitAction">One-time init action.</param>
	public ObjectPool (int initialBufferSize, Action<T> ResetAction = null,
	                   Action<T> OnetimeInitAction = null) 
	{
		m_objectStack = new Stack<T>(initialBufferSize);
		m_resetAction = ResetAction;
		m_onetimeInitAction = OnetimeInitAction;
	}
	
	/// <summary>
	/// Returns a fresh object from the pool, resetting/initializing it if necessary
	/// </summary>
	public T New () 
	{
		// if our stack isn't empty
		if (m_objectStack.Count > 0) 
		{
			// pop one of those bad boys off the stack so we can use it
			T t = m_objectStack.Pop ();
			
			// call the reset action if one has been provided
			if (m_resetAction != null)
				m_resetAction(t);
				
			return t;
		} 
		// nothing on the stack? 
		else {
			// ok, let's make a new one
			T t = new T();
			
			// if we need to do something with a new T
			if (m_onetimeInitAction != null)
				m_onetimeInitAction(t);
				
			return t;
		}
	}
	
	/// <summary>
	/// Store the specified instance on the stack
	/// </summary>
	/// <param name="obj">Object to throw on the stack</param>
	public void Store(T obj)
	{
		m_objectStack.Push(obj);
	}
}