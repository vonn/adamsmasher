﻿using UnityEngine;
using System.Collections;

public class CrosshairControl : MonoBehaviour {
	[HideInInspector]
	public bool armed = false;
	public SpriteRenderer sprite;
	public Color armedColor = new Color(1.0f,0.0f,0.0f, 0.2f);
	public Color unarmedColor = new Color(0.5f,0.5f,0.5f, 0.2f);

	void Start() {

	}

	void OnGUI () {

	}

	void Update() {
		if (armed) {
			sprite.color = Color.red;
		} else {
			sprite.color = Color.white;
		}
	}

	void OnTriggerEnter2D(Collider2D col){ 
		if (col.gameObject.tag == "Target") { 
			armed = true;
		}
	}

	void OnTriggerStay2D(Collider2D col){ 
		if (col.gameObject.tag == "Target") { 
			armed = true;
		}
	}

	void OnTriggerExit2D(Collider2D col){ 
		if (col.gameObject.tag == "Target") { 
			armed = false;
		}
	}
}
