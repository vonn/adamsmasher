﻿using UnityEngine;
using System.Collections;
public class CameraShake : MonoBehaviour {
	
	private Vector3 originPosition;	
	private Quaternion originRotation;	
	private float decay;	
	private float intensity;	
	private bool shaking;	
	private Transform _transform;

	public Vector2 DecayAndIntensity;	// stores info about shake (decay, intensity)
	
	void OnGUI (){
		// button to debug
		/*if (GUI.Button (new Rect (20,40,80,20), "Shake")){			
			Shake ();			
		}	*/	
	}

	void Awake(){
		resetVars ();
		//Debug.Log("Setting things || Decay: " + decay + ", Intensity: " + intensity);
	}

	void resetVars() {
		decay = DecayAndIntensity.x;
		intensity = DecayAndIntensity.y;
	}

	void OnEnable() {		
		_transform = transform;		
	}
	
	void Update (){
		
		if(!shaking)			
			return;
		
		if (intensity > 0f){			
			_transform.localPosition = originPosition + Random.insideUnitSphere * intensity;
			
			_transform.localRotation = new Quaternion(				
				originRotation.x + Random.Range (-intensity,intensity) * .2f,				
				originRotation.y + Random.Range (-intensity,intensity) * .2f,				
				originRotation.z + Random.Range (-intensity,intensity) * .2f,				
				originRotation.w + Random.Range (-intensity,intensity) * .2f);
			intensity -= decay;
		} else {
			shaking = false;
			_transform.localPosition = originPosition;
			_transform.localRotation = originRotation;
			// and reset the shake data
			resetVars();
			//Debug.Log("Decay: " + decay + ", Intensity: " + intensity);
		}
	}
	
	public void Shake(){
		if(!shaking) {
			originPosition = _transform.localPosition;
			originRotation = _transform.localRotation;
		}
		shaking = true;
	}
	
}