﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	private bool charge = false;			// Condition for whether the player should jump.
	private Transform target;				// what we're gonna charge to
	private CameraShake camShaker; 			// handle on the camerashake controller
	private float nextCharge = 0.0f;		// time we can charge next
	private string searchTag = "Target";
	
	public float chargeRadius;              // max radius Adam can charge at
	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public float concForce = 5f;			// The concussive force of collision (determines how far player bounces)
	public float concNoise = .5f;			// the spread of the bounce angle
	public float chargeRate = 0.5f;			// rate at which we can charge (once every half-second)
	public AudioClip[] dashClips;			// sounds to choose from when charging	
	//public CrosshairControl xhair;			// my crosshair
	
	private Camera cam;                     // handle to the cm
	public float minX, maxX;                // to limit horizontal movement
	
	float angle;
	Vector3 mouse_pos;
	Vector3 object_pos;
	
	void Awake()
	{
		// Setting up references.
		camShaker = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake> ();
		cam = Camera.main;
	}
	
	void Update()
	{	
		if (Input.GetButtonDown ("Fire1")
		    && Time.time > nextCharge
		    /*&& xhair.armed*/) 
		{
			// set up time of next charge
			nextCharge = Time.time + chargeRate;

			// find the nearest target
			target = GetNearestTaggedObject();

			if (target != null 
			    && target.GetComponent<AtomController>().state == AtomState.BAD) 
			{
				charge = true;
				//xhair.armed = false;
			}
		}
		else if (Input.GetKeyDown(KeyCode.R))
			Application.LoadLevel("Level");
	}
	
	void FixedUpdate ()
	{
		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		//anim.SetFloat("Speed", Mathf.Abs(h));
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * moveForce);
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);
		
		// limit horizontal movement
		/*if (transform.position.x <= minX)
			transform.position = new Vector2(minX, transform.position.y);
		else if (transform.position.x >= maxX)
			transform.position = new Vector2(maxX, transform.position.y);*/
			
		// vertical movement
		if(v * rigidbody2D.velocity.y < maxSpeed)
			rigidbody2D.AddForce(Vector2.up * v * moveForce);
		if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed)
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);

		Vector3 top = cam.ViewportToWorldPoint(new Vector3(0, 1, 0));
		
		float topOfView = cam.ViewportToWorldPoint(new Vector3(0, 1, 0)).y;
		float botOfView = cam.ViewportToWorldPoint(new Vector3(0, 0, 0)).y;

		/*// limit vert movement
		if (transform.position.y <= botOfView)
			transform.position = new Vector2(transform.position.x, botOfView);
		else if (transform.position.y >= topOfView)
			transform.position = new Vector2(transform.position.x, topOfView);*/
			
		// If the player should charge...
		if(charge)
		{
			Charge();
		}
	}
	
	/// <summary>
	/// Gets the nearest tagged object.
	/// </summary>
	/// <returns>The nearest bad atom</returns>
	Transform GetNearestTaggedObject() 
	{
		float nearestDistanceSqr = Mathf.Infinity;
		GameObject[] taggedGameObjects = GameObject.FindGameObjectsWithTag(searchTag); 
		Transform nearestObj = null;
		
		// loop through each tagged object, remembering nearest one found
		for (int i = 0; i < taggedGameObjects.Length; i++) 
		{
			var objectPos = taggedGameObjects[i].transform.position;
			var distanceSqr = (objectPos - transform.position).sqrMagnitude;
			
			bool inRange = distanceSqr <= chargeRadius*chargeRadius;
			bool isBad = taggedGameObjects[i].GetComponent<AtomController>().state == AtomState.BAD;
			
			//Debug.Log (distanceSqr + " is in range? " + inRange);
			// only consider bad atoms in range
			if ( !isBad || !inRange)
				continue;
			
			if (distanceSqr < nearestDistanceSqr) 
			{
				nearestObj = taggedGameObjects[i].transform;
				nearestDistanceSqr = distanceSqr;
			}
		}
		
		return nearestObj;
	}

	/// <summary>
	/// Causes the player to charge at the nearest atom
	/// </summary>
	public void Charge() {
		// Set the charge animator trigger parameter.
		//anim.SetTrigger("Charge");
		
		// move to target
		transform.position = target.position;
		
		// and destroy it (TODO: explosion, sound, etc.)
		//Destroy(target.gameObject);
		target.GetComponent<AtomController>().Die();
		
		// Play a random audio clip.
		int i = Random.Range(0, dashClips.Length);
		AudioSource.PlayClipAtPoint(dashClips[i], transform.position);
		
		// shake the cam
		camShaker.Shake();
		
		// reset gravity so we don't fall fast from here
		rigidbody2D.velocity = Vector2.up * concForce;
		rigidbody2D.angularVelocity = 0.0f;
		//rigidbody2D.Sleep();
		
		// Add a vertical force to the player.
		Vector3 randTheta = Random.insideUnitSphere * concNoise;  
		Vector2 direction = Vector2.up + (Vector2)randTheta;
		//rigidbody2D.AddForce(new Vector2(0f, concForce));
		rigidbody2D.AddForce(new Vector2(direction.x*concForce, direction.y*concForce));
		
		// Make sure the player can't jump again until the jump conditions from Update are satisfied.
		charge = false;
	}
}